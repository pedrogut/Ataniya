jQuery(document).ready(function($) {
  "use strict";
  var $contactForm = $('form.contactForm');
  //Contact
  $contactForm.submit(function(e) {
    e.preventDefault();
    var f = $(this).find('.form-group'),
      ferror = false,
      emailExp = /^[^\s()<>@,;:\/]+@\w[\w\.-]+\.[a-z]{2,}$/i,
      phoneExp = /^(\s*|\d+)$/;

    f.children('input').each(function() { // run all inputs

      var i = $(this); // current input
      var rule = i.attr('data-rule');

      if (rule !== undefined) {
        var ierror = false; // error flag for current input
        var pos = rule.indexOf(':', 0);
        if (pos >= 0) {
          var exp = rule.substr(pos + 1, rule.length);
          rule = rule.substr(0, pos);
        } else {
          rule = rule.substr(pos + 1, rule.length);
        }

        switch (rule) {
          case 'required':
            if (i.val() === '') {
              ferror = ierror = true;
            }
            break;

          case 'minlen':
            if (i.val().length < parseInt(exp)) {
              ferror = ierror = true;
            }
            break;

          case 'email':
            if (!emailExp.test(i.val())) {
              ferror = ierror = true;
            }
            break;

          case 'phone':
            if (!phoneExp.test(i.val())) {
              ferror = ierror = true;
            }
            break;

          case 'checked':
            if (! i.is(':checked')) {
              ferror = ierror = true;
            }
            break;

          case 'regexp':
            exp = new RegExp(exp);
            if (!exp.test(i.val())) {
              ferror = ierror = true;
            }
            break;
        }
        i.next('.validation').html((ierror ? (i.attr('data-msg') !== undefined ? i.attr('data-msg') : 'wrong Input') : '')).show('blind');
      }
    });
    f.children('textarea').each(function() { // run all inputs

      var i = $(this); // current input
      var rule = i.attr('data-rule');

      if (rule !== undefined) {
        var ierror = false; // error flag for current input
        var pos = rule.indexOf(':', 0);
        if (pos >= 0) {
          var exp = rule.substr(pos + 1, rule.length);
          rule = rule.substr(0, pos);
        } else {
          rule = rule.substr(pos + 1, rule.length);
        }

        switch (rule) {
          case 'required':
            if (i.val() === '') {
              ferror = ierror = true;
            }
            break;

          case 'minlen':
            if (i.val().length < parseInt(exp)) {
              ferror = ierror = true;
            }
            break;
        }
        i.next('.validation').html((ierror ? (i.attr('data-msg') != undefined ? i.attr('data-msg') : 'wrong Input') : '')).show('blind');
      }
    });
    if (ferror) return false;
/*     else var str = $(this).serialize();
    var action = $(this).attr('action');
    if( ! action ) {
      action = 'contactform/contactform.php';
    }
    $.ajax({
      type: "POST",
      url: action,
      data: str,
      success: function(msg) {
        // alert(msg);
        if (msg == 'OK') {
          $("#sendmessage").addClass("show");
          $("#errormessage").removeClass("show");
          $('.contactForm').find("input, textarea").val("");
        } else {
          $("#sendmessage").removeClass("show");
          $("#errormessage").addClass("show");
          $('#errormessage').html(msg);
        }

      }
    }); */
    else var str = $(this).serialize();
    $.ajax({
      url: 'https://usebasin.com/f/f69f2fe2a86e.json',
      method: 'POST',
      data: str,
      dataType: 'json',
      beforeSend: function () {
          $contactForm.find('.messages').html('<div class="alert alert--success"  style="background-color: #0063A5;color: white;opacity: 0.9">Enviando mensaje</div>');
      },
      success: function (data) {
          $contactForm.find('.alert--loading').hide();
          $contactForm.find('.messages').html('<div class="alert alert--success"  style="background-color: forestgreen;color: white;opacity: 0.9"><button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="color: white">&times;</button>¡Mensaje enviado!</div>');
          $contactForm[0].reset();
      },
      error: function (err) {
          $contactForm.find('.alert--loading').hide();
          $contactForm.find('.messages').html('<div class="alert alert--success"  style="background-color: darkred;color: white;opacity: 0.9"><button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="color: white">&times;</button>No se pudo enviar el mensaje. Verifique la información ingresada y/o la verificación humana (cartel que dice "No soy un robot")</div>');
      }
  });
    return false;
  });

});
